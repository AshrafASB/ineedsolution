<?php

return [
    /**
     * Control if the seeder should create a user per role while seeding the data.
     */
    'create_users' => false,

    /**
     * Control if all the laratrust tables should be truncated before running the seeder.
     */
    'truncate_tables' => true,

    'roles_structure' => [

        'super_admin' => [
            'users' => 'c,r,u,d',
            'categories' => 'c,r,u,d',
            'roles'=>'c,r,u,d',
            'settings'=>'c,r,u,d',
            'contact_us'=>'c,r,u,d',
            'posts'=>'c,r,u,d',
            'replies'=>'c,r,u,d',
            'consultations'=>'c,r,u,d'



        ],
        'people'=>[
            'posts'=>'c,r,u,d',
            'replies'=>'c,r,u,d'
        ],
        'experts'=>[
            'posts'=>'c,r,u,d',
            'consultations'=>'c,r,u,d',
            'replies'=>'c,r,u,d'
        ],
        'admin'=>[],
         'user'=>[]

    ],
//    'permission_structure'=>[
//
//    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
