<?php

namespace App\Http\Controllers;

use App\Category;
use App\ContactUs;
use App\Posts;
use App\Reply;
use App\WhoAreWe;
use Illuminate\Http\Request;

class LandingPageController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "welcome";
    }

    public function Landing()
    {
//      posts
        $posts = Posts::OrderBy('id','Desc')
                        ->simplePaginate(7);
//      Reply
        $reply = Reply::all();

        return view($this->path,compact(['posts','reply']));

//        return view($this->path);

    }//end of Landing

    public function detailsPost (Posts $post){
        return view('detailsPost',compact('post'));
    }//end of detailsPost

    public function store(Request $request)
    {
//        dd($request->all());
        $data = $request->except('post_id','to');
        $data['post_id'] = $request->post_id;
        $data['to'] = auth()->user()->id;
        Reply::create($data);
        session()->flash('success',__('تم ارسال نصيحتك بنجاح، شكراً لتعاونك'));
        return view('thanks');
    }//end of store























//
//    public function store(Request $request)
//    {
////        dd(1);
//        $request->validate([
//            'email' => 'required|unique:benefactors,email',
//        ]);
//        Benefactor::create($request->all());
//        session()->flash('success', __('site.DataAddSuccessfully'));
//        return view('thanksBooking');
//    }//end of store
//
//
//    public function requestNeedy()
//    {
//
//        return view('neediesb');
//    }//end of donation
//
//    public function requestNeedyStore(Request $request)
//    {
////        dd($request->all());
//        $request->validate([
//            'userName' => 'required|unique:needies,userName',
//        ]);
////        $request->only('name','userName','phone','city','address','family_number','notes');
//        $data = $request->except(['id_photo', 'supply_card_photo']);
//
//        if ($request->hasFile('id_photo')) {
//            $id_photo = $request->id_photo->store('images', 'public');
//            $data['id_photo'] = $id_photo;
//        }
//        if ($request->hasFile('supply_card_photo')) {
//            $supply_card_photo = $request->supply_card_photo->store('images', 'public');
//            $data['supply_card_photo'] = $supply_card_photo;
//        }
//
//        Needy::create($data);
////        return view('neediesb');
//        return view('thanksBooking');
//    }//end of donation
//
//
//    public function contact(Request $request){
//        ContactUs::create($request->all());
//        session()->flash('success',__('site.DataAddSuccessfully'));
//        return view('thanks');
//    }//end of


}
