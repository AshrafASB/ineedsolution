<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Needy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class NeedyController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.needies.";

        //Permissions
        $this->middleware('permission:read_needies')->only(['index']);
//        $this->middleware('permission:create_needies')->only(['create','store']);
        $this->middleware('permission:update_needies')->only(['edit','update']);
        $this->middleware('permission:delete_needies')->only(['destroy']);

    }

    public function index()
    {
        $needies = Needy::WhenSearch(request()->search)->paginate(5);
        return view($this->path.'index',compact('needies'));
    }//end of index

    public function create()
    {
       return view($this->path.'create');
    }//end of create

    public function store(Request $request)
    {
//        dd($request->all());
        $request->validate([
            'userName' => 'required|unique:needies,userName',
        ]);
//        $request->only('name','userName','phone','city','address','family_number','notes');
         $data = $request->except(['id_photo','supply_card_photo']);

        if ($request->hasFile('id_photo')){
            $id_photo = $request->id_photo->store('images','public');
            $data['id_photo'] = $id_photo;
        }
       if ($request->hasFile('supply_card_photo')){
            $supply_card_photo = $request->supply_card_photo->store('images','public');
            $data['supply_card_photo'] = $supply_card_photo;
        }

        Needy::create($data);
        session()->flash('success',__('site.DataAddSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of store

    public function show(Needy $needy)
    {
        $details = $needy;
        return view($this->path.'create',compact('details'));
    }//end of show

    public function edit(Needy $needy)
    {
        return view($this->path.'create',compact('needy'));
    }//end of edit

    public function update(Request $request, Needy $needy)
    {
        $request->validate([
            'userName' => 'required|unique:needies,userName,'.$needy->id,
        ]);
        $data = $request->except(['id_photo','supply_card_photo']);
        if ($request->hasFile('id_photo')){
            $id_photo = $request->id_photo->store('images','public');
            Storage::disk('public')->delete($needy->id_photo);
            $data['id_photo'] = $id_photo;
        }
        if ($request->hasFile('supply_card_photo')){
            $supply_card_photo = $request->supply_card_photo->store('images','public');
            Storage::disk('public')->delete($needy->supply_card_photo);
            $data['supply_card_photo'] = $supply_card_photo;
        }

        $needy->update($data);
        session()->flash('success',__('site.DataUpdatedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of update

    public function destroy(Needy $needy)
    {
        $needy->delete();
        session()->flash('success',__('site.DataDeletedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of destroy
}
