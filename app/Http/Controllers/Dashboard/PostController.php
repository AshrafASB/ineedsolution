<?php

namespace App\Http\Controllers\Dashboard;

use App\Posts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.posts.";

        //Permissions
        $this->middleware('permission:read_posts')->only(['index']);
        $this->middleware('permission:create_posts')->only(['create','store']);
        $this->middleware('permission:update_posts')->only(['edit','update']);
        $this->middleware('permission:delete_posts')->only(['destroy']);

    }

    public function index()
    {

        $posts = Posts::WhenSearch(request()->search)
            ->OrderBy('id','Desc')
            ->paginate(5);

        return view($this->path.'index',compact('posts'));
    }//end of index

    public function create()
    {
        return view($this->path.'create');
    }//end of create

    public function store(Request $request)
    {
//        $request->validate([
//            'user_id' => 'required|unique:posts,user_id',
//        ]);
        $data= $request->except('image');
        $data['user_id'] = auth()->user()->id;
        if ($request->hasFile('image')){
            $image = $request->image->store('images','public');
            $data['image'] = $image;
        }
        Posts::create($data);
        session()->flash('success',__('site.DataAddSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of store

    public function show($id)
    {
        //
    }//end of show

    public function edit(Posts $post)
    {
        return view($this->path.'create',compact('post'));
    }//end of edit

    public function update(Request $request, Posts $post)
    {

//        $request->validate([
//            'user_id' => 'required|unique:posts,user_id,'.$post->id,
//        ]);

        $data = $request->except('image');
        $data['user_id'] = auth()->user()->id;
        if ($request->hasFile('image')){
            $image = $request->image->store('images','public');
            Storage::disk('public')->delete($post->image);
            $data['image'] = $image;
        }
        $post->update($data);
        session()->flash('success',__('site.DataUpdatedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of update

    public function destroy(Posts $post)
    {
        $post->delete();
        session()->flash('success',__('site.DataDeletedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of destroy
}
