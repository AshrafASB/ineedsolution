<?php

namespace App\Http\Controllers\Dashboard;

use App\Classification;
use App\Http\Controllers\Controller;
use App\Needy;
use Illuminate\Http\Request;

class ClassificationController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.classifications.";

        //Permissions
        $this->middleware('permission:read_classifications')->only(['index']);
        $this->middleware('permission:create_classifications')->only(['create','store']);
        $this->middleware('permission:update_classifications')->only(['edit','update']);
        $this->middleware('permission:delete_classifications')->only(['destroy']);

    }

    public function index()
    {
        $classifications = Classification::WhenSearch(request()->search)
            ->with(['needies','needy'])
            ->paginate(5);
        return view($this->path.'index',compact('classifications'));
    }//end of index

    public function create()
    {
        $needies = Needy::all();
        return view($this->path.'create',compact('needies'));
    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'needy_id' => 'required|unique:classifications,needy_id',
        ]);
        Classification::create($request->all());
        session()->flash('success',__('site.DataAddSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of store

    public function show($id)
    {
        //
    }//end of show

    public function edit(Classification $classification)
    {
        $needies = Needy::all();
        return view($this->path.'create',compact('classification','needies'));
    }//end of edit

    public function update(Request $request, Classification $classification)
    {
        $request->validate([
            'needy_id' => 'required|unique:classifications,needy_id,'.$classification->id,
        ]);
        $classification->update($request->all());
        session()->flash('success',__('site.DataUpdatedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of update

    public function destroy(Classification $classification)
    {
        $classification->delete();
        session()->flash('success',__('site.DataDeletedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of destroy
}
