<?php

namespace App\Http\Controllers\Dashboard;

use App\Reply;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReplyController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.replies.";

        //Permissions
        $this->middleware('permission:read_replies')->only(['index']);
        $this->middleware('permission:create_replies')->only(['create','store']);
        $this->middleware('permission:update_replies')->only(['edit','update']);
        $this->middleware('permission:delete_replies')->only(['destroy']);

    }

    public function index()
    {
        $replies = Reply::WhenSearch(request()->search)
            ->OrderBy('id','Desc')
            ->paginate(10);
        return view($this->path.'index',compact('replies'));
    }//end of index

    public function create()
    {
        return view($this->path.'create');
    }//end of create

    public function store(Request $request)
    {
//        $request->validate([
//            'email' => 'required|unique:replies,email',
//        ]);
//        $data = except([]);

        Reply::create($request->all());
        session()->flash('success',__('site.DataAddSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of store

    public function show($id)
    {
        //
    }//end of show

    public function edit(Reply $reply)
    {
        return view($this->path.'create',compact('reply'));
    }//end of edit

    public function update(Request $request, Reply $reply)
    {
//        $request->validate([
//            'email' => 'required|unique:replies,email,'.$reply->id,
//        ]);
        $reply->update($request->all());
        session()->flash('success',__('site.DataUpdatedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of update

    public function destroy(Reply $reply)
    {
        $reply->delete();
        session()->flash('success',__('site.DataDeletedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of destroy
}
