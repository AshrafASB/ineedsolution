<?php

namespace App\Http\Controllers\Dashboard;

use App\Benefactor;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BenefactorController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.benefactors.";

        //Permissions
        $this->middleware('permission:read_benefactors')->only(['index']);
        $this->middleware('permission:create_benefactors')->only(['create','store']);
        $this->middleware('permission:update_benefactors')->only(['edit','update']);
        $this->middleware('permission:delete_benefactors')->only(['destroy']);

    }

    public function index()
    {
        $benefactors = Benefactor::WhenSearch(request()->search)->paginate(5);
        return view($this->path.'index',compact('benefactors'));
    }//end of index

    public function create()
    {
        return view($this->path.'create');
    }//end of create

    public function store(Request $request)
    {
//        $request->validate([
//            'email' => 'required|unique:benefactors,email',
//        ]);
        Benefactor::create($request->all());
        session()->flash('success',__('site.DataAddSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of store

    public function show($id)
    {
        //
    }//end of show

    public function edit(Benefactor $benefactor)
    {
        return view($this->path.'create',compact('benefactor'));
    }//end of edit

    public function update(Request $request, Benefactor $benefactor)
    {
//        $request->validate([
//            'email' => 'required|unique:benefactors,email,'.$benefactor->id,
//        ]);
        $benefactor->update($request->all());
        session()->flash('success',__('site.DataUpdatedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of update

    public function destroy(Benefactor $benefactor)
    {
        $benefactor->delete();
        session()->flash('success',__('site.DataDeletedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of destroy
}
