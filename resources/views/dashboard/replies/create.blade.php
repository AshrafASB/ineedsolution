@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
            @if(isset($reply)  )
                <h1>
                    <i class="fa fa-edit">
                        {{ __('site.Reply Details')}}
                    </i>
               </h1>
            @else
                <h1>
                    <i class="fa fa-plus">
                      {{__('site.Add Reply') }}
                    </i>
                </h1>
            @endif

        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-list"></i></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">{{__('site.Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.replies.index')}}">{{__('site.Reply')}}</a></li>
            @if(isset($reply))
                <li class="breadcrumb-item">{{__('site.Reply Details')}}</li>
            @else
                <li class="breadcrumb-item">{{__('site.Add Reply')}}</li>
            @endif
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
{{--             <form action="{{isset($reply)?route('dashboard.replies.update',$reply->id):route('dashboard.replies.store')}}" method="post">--}}
                 @csrf
                 @if(isset($reply))
                     @method('put')
                 @else
                     @method('post')
                 @endif

                 @include('dashboard.partials._errors')

                 <div class="form-group">
                     <label>{{__('site.name')}} :</label>
                     <input type="text" name="name" class="form-control" disabled value="{{isset($reply)?$reply->name:""}}">
                 </div>

                 <div class="form-group">
                     <label>{{__('site.email')}} :</label>
                     <input type="text" name="email" class="form-control" disabled value="{{isset($reply)?$reply->email:""}}">
                 </div>

                 <div class="form-group">
                     <label>{{__('site.message')}} :</label>
                     <textarea name="message" cols="30" rows="10" disabled class="form-control">{{isset($reply)?$reply->message:""}}</textarea>
                 </div>

                 <div class="form-group">
                     <a class="btn btn-primary" href="{{route('dashboard.replies.index')}}">Back</a>
{{--                     <button type="submit" class="btn btn-primary">--}}
{{--                         @if( isset($reply) )--}}
{{--                             <i class="fa fa-edit"></i>--}}
{{--                             {{__('site.Update')}}--}}
{{--                         @else--}}
{{--                             <i class="fa fa-plus"></i>--}}
{{--                             {{__('site.Add')}}--}}
{{--                         @endif--}}

{{--                     </button>--}}
                 </div>
{{--             </form>--}}

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}


    </div>{{--end-of-tile mb-4--}}


@endsection
