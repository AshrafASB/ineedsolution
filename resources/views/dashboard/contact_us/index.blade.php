@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
                <h1><i class="fa fa-list"></i> {{__('site.Contact Us')}} </h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">Dashboard</a></li>
            <li class="breadcrumb-item"> {{__('site.Contact Us')}}</li>
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
                {{-- this form for Search button                --}}
                <form action="" >
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="search" autofocus class="form-control" placeholder="Search" value="{{request()->search}}">
                            </div>
                        </div>{{-- end-of-col-4 --}}


                        <div class="col-md-4">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i>Search</button>

                            </div>
                        </div>{{-- end-of-col-4 --}}


                    </div>{{-- end-of-row --}}
                </form>{{-- end-of-form --}}

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}

        <div class="row">
            <div class="col-md-12">
                <hr>
                @if($contact_us->count() > 0 )
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('site.name')}}</th>
                            <th>{{__('site.phone')}}</th>
                            <th>{{__('site.Message')}}</th>
                            <th>{{__('site.Received in')}}</th>

                            <th>{{__('site.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($contact_us as $index=>$contact)
                            <tr>
                                <td>{{++$index}}</td>
                                <td> {{$contact->name}} </td>
                                <td> {{$contact->phone}} </td>
                                <td style="width: 200px"> {{\Illuminate\Support\Str::limit($contact->message, 100)}} </td>
                                <td> {{$contact->created_at->diffForHumans()}} </td>

                                <td>
{{--                                    --}}{{--Edit buttom--}}
{{--                                    @if(auth()->user()->hasPermission('update_contact_us'))--}}
{{--                                        <a href="{{route('dashboard.contact_us.edit', $contact->id)}}" class="btn btn-warning btn-sm"><i class="fa fa-edit">Edit</i></a>--}}
{{--                                    @else--}}
{{--                                        <a href="#" disabled="" class="btn btn-warning btn-sm"><i class="fa fa-edit">{{__('site.Edit')}}</i></a>--}}
{{--                                    @endif--}}

                                    {{--Delete buttom--}}
                                    @if(auth()->user()->hasPermission('delete_contact_us'))
                                        <form action="{{route('dashboard.contact_us.destroy', $contact->id)}}" method="post" style="display: inline-block">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i>{{__('site.Delete')}}</button>
                                        </form>
                                    @else
                                        <a href="#" disabled="" class="btn btn-danger btn-sm"><i class="fa fa-edit">{{__('site.Delete')}}</i></a>
                                    @endif

                                </td>
                            </tr>
                        @endforeach

                        </tbody>

                    </table>
                    {{$contact_us->appends(request()->query())->links()}}
                @else
                    <h3 style="font-weight: 400; text-align: center"> No Record Found</h3>
                @endif
            </div>
        </div>
    </div>{{--end-of-tile mb-4--}}


@endsection
