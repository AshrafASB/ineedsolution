@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
            @if(isset($post)  )
                <h1>
                    <i class="fa fa-edit">
                        {{ __('site.Update Post')}}
                    </i>
               </h1>
            @else
                <h1>
                    <i class="fa fa-plus">
                      {{__('site.Add Post') }}
                    </i>
                </h1>
            @endif

        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-list"></i></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">{{__('site.Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.posts.index')}}">{{__('site.Post')}}</a></li>
            @if(isset($post))
                <li class="breadcrumb-item">{{__('site.Update Post')}}</li>
            @else
                <li class="breadcrumb-item">{{__('site.Add Post')}}</li>
            @endif
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
             <form action="{{isset($post)?route('dashboard.posts.update',$post->id):route('dashboard.posts.store')}}" method="post" enctype="multipart/form-data">
                 @csrf
                 @if(isset($post))
                     @method('put')
                 @else
                     @method('post')
                 @endif

                 @include('dashboard.partials._errors')

                 <div class="form-group">
                     <label>{{__('site.title')}} :</label>
{{--                     <textarea name="title" cols="30" rows="10"  class="form-control">{{isset($post)?$post->title:""}}</textarea>--}}
                     <input type="text" name="title" class="form-control" value="{{isset($post)?$post->title:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.body')}} :</label>
                     <textarea name="body" cols="30" rows="10"  class="form-control">{{isset($post)?$post->body:""}}</textarea>
                 </div>
                 <div class="form-group">
                     <label>{{__('site.image')}} :</label>
                     <input type="file" name="image" class="form-control" value="{{isset($post)?$post->image:""}}">
                 </div>

                 <div class="form-group">
                     <button type="submit" class="btn btn-primary">
                         @if( isset($post) )
                             <i class="fa fa-edit"></i>
                             {{__('site.Update')}}
                         @else
                             <i class="fa fa-plus"></i>
                             {{__('site.Add')}}
                         @endif

                     </button>
                 </div>
             </form>

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}


    </div>{{--end-of-tile mb-4--}}


@endsection
