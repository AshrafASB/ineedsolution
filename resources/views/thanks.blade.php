@include('header')
<div class="main-wrapper">
    <article class="blog-post px-3 py-5 p-md-5">
        <div class="container">
            <header class="blog-post-header">
                <h2 class="title mb-2">شكرا لك على نصيحتك </h2>
                <div class="meta mb-3"><span class="date" style="justify-content: center">لقد تلقى صاحب المشكلة نصيحتك</span></div>
                <nav class="blog-nav nav nav-justified my-5">
                    <a class="nav-link-next nav-item nav-link rounded" href="{{route('welcome')}}">الرجوع للرئيسية<i class="arrow-next fas fa-long-arrow-alt-right"></i></a>
                </nav>
                <a href="https://made4dev.com"><img class="img-fluid" style="border: #00E578 solid 2px;" src="dashboard_files/images/thanks.jpg" alt="image"></a>

            </header>
        </div>
    </article>
</div>
@include('footer')
