@include('header')
<!-- ***** Welcome Area Start ***** -->
<div class="welcome-area" id="welcome">
    <!-- ***** Header Text Start ***** -->
    <div class="header-text">
        <div class="container">
            <div class="row">
                <div class="offset-xl-2 col-xl-12 offset-lg-2 col-lg-8 col-md-12 col-sm-12">
                    <h1 style="margin-right:40%;margin-top: 5%">Welcome plz<br> filling in your donation data </h1>
{{--                    <h1 style="margin-right:40%">^-^</h1>--}}
                    <div class="col-lg-8 col-md-6 col-sm-12" >
                        <div class="contact-form" style="margin-right: 5%">
                            @include('dashboard.partials._errors')
                            <form id="contact" action="{{route('store')}}" method="post">
                                @csrf
                                @method('post')
                                <div class="row">
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <fieldset>
                                            <input name="name" type="text" class="form-control" id="name" placeholder="Your name" required="">
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <fieldset>
                                            <input name="email" type="email" class="form-control" id="email" placeholder="Your Email" required="">
                                        </fieldset>
                                    </div><br>
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <fieldset>
                                            <input name="phone" type="text" class="form-control" id="phone" placeholder="Your Phone" required="">
                                        </fieldset>
                                    </div>

                                    <div class="form-group">
{{--                                        <label>{{__('site.Type donation')}} :</label>--}}
                                        <select name="type_donation" class="form-control">
                                            <option value="0"> Type donation </option>
                                            <option value="1"> {{__('site.monthly') }}</option>
                                            <option value="2" > {{__('site.yearly') }}</option>
                                            <option value="3"> {{__('site.once') }}</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <fieldset>
                                            <input name="amount" type="text" class="form-control" id="email" placeholder="The amount you wish to donate" >
                                        </fieldset>
                                    </div>

                                    <div class="form-group">
{{--                                        <label>{{__('site.Currency')}} :</label>--}}
                                        <select name="currency" class="form-control">
                                            <option value="0"> Currency </option>
                                            <option value="1"> {{__('site.shekel') }}</option>
                                            <option value="2" > {{__('site.Dollar') }}</option>
                                            <option value="3"> {{__('site.Jordanian Dinar') }}</option>
                                            <option value="4"> {{__('site.euro') }}</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-12">
                                        <fieldset>
                                            <textarea name="message" rows="6" class="form-control" id="message" placeholder="Your Message" required=""></textarea>
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-12">
                                        <fieldset>
                                            <button type="submit" id="form-submit" class="main-button" style="background: #0B90C4;">Send</button>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
{{--                    <a href="{{route('home')}}" class="main-button-slider">Back to Main Page</a>--}}
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Header Text End ***** -->
</div>
<!-- ***** Welcome Area End ***** -->




<!-- ***** Features Small Start ***** -->
<section class="section home-feature">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

            </div>
        </div>
    </div>
</section>
<!-- ***** Features Small End ***** -->


@include('footer')
