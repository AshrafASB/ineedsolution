@include('header')
<!-- ***** Welcome Area Start ***** -->
<div class="welcome-area" id="welcome">
    <!-- ***** Header Text Start ***** -->
    <div class="header-text">
        <div class="container">
            <div class="row">
                <div class="offset-xl-2 col-xl-12 offset-lg-2 col-lg-8 col-md-12 col-sm-12">
                    <h1 style="margin-right:40%;margin-top: 5%">Fill Your Data </h1>
                    {{--                    <h1 style="margin-right:40%">^-^</h1>--}}
                    <div class="col-lg-8 col-md-6 col-sm-12" >
                        <div class="contact-form" style="margin-right: 5%">
                            @include('dashboard.partials._errors')
                            <form id="contact" action="{{route('requestNeedyStore')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                @method('post')
                                <div class="row">
                                    <div class="col-lg-4 col-md-6 col-sm-6">
                                        <fieldset>
                                            <input name="name" type="text" class="form-control" id="name" placeholder="Your name" required="">
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-6">
                                        <fieldset>
                                            <input name="userName" type="userName" class="form-control" id="userName" placeholder="Your Email" required="">
                                        </fieldset>
                                    </div><br>
                                    <div class="col-lg-4 col-md-6 col-sm-6">
                                        <fieldset>
                                            <input name="phone" type="text" class="form-control" id="phone" placeholder="Your Phone" required="">
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-6">
                                        <fieldset>
                                            <input name="phone" type="text" class="form-control"  placeholder="Enter Your phone" >
                                        </fieldset>
                                    </div>

                                    <div class="col-lg-4 col-md-6 col-sm-6">
                                        <fieldset>
                                            <input name="city" type="text" class="form-control"  placeholder="Enter Your city" >
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-6">
                                        <fieldset>
                                            <input name="address" type="text" class="form-control"  placeholder="Enter Your address" >
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-6">
                                        <fieldset>
                                            <input name="family_number" type="number" class="form-control"  placeholder="Your family Number" >
                                        </fieldset>
                                    </div>
                                    <br>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        {{--                                       <label for="">Upload Your  Photo ID</label>--}}
                                        <fieldset>
                                            <input name="id_photo" type="file" class="form-control"  placeholder="Upload Your Photo ID" >
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        {{--                                      <label for="">Upload Your Supply Card Photo</label>--}}
                                        <fieldset>
                                            <input name="supply_card_photo" type="file" class="form-control"  placeholder="Upload Your Supply Card Photo" >
                                        </fieldset>
                                    </div>



                                    <div class="col-lg-12">
                                        <fieldset>
                                            <textarea name="notes" rows="3" class="form-control" id="message" placeholder="Your Message" required=""></textarea>
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-12">
                                        <fieldset>
                                            <button type="submit" id="form-submit" class="main-button" style="background: #0B90C4;">Send</button>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    {{--                    <a href="{{route('home')}}" class="main-button-slider">Back to Main Page</a>--}}
                </div>

            </div>
        </div>
    </div>
    <!-- ***** Header Text End ***** -->
</div>
<!-- ***** Welcome Area End ***** -->




{{--<!-- ***** Features Small Start ***** -->--}}
{{--<section class="section home-feature">--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-lg-12">--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}
{{--<!-- ***** Features Small End ***** -->--}}


@include('footer')
