@include('header')

<div class="main-wrapper">
    <section class="cta-section theme-bg-light py-5">
        <div class="container text-center">
            <h2 class="heading">أهلا وسهلا بيك في موقع أريد حلاً </h2>
            <div class="intro">هنا تجد حلول أكثر لمشاكلك والعواقب التي تواجهك</div>
            <div class="intro">إنضم لعائلتنا لتستطيع نشر مشكلتك عبر الربط النالي <span><a href="#">أنشئ حساب جديد</a></span></div>
{{--            <form class="signup-form form-inline justify-content-center pt-3">--}}
{{--                <div class="form-group">--}}
{{--                    <label class="sr-only" for="semail">Your email</label>--}}
{{--                    <input type="email" id="semail" name="semail1" class="form-control mr-md-1 semail" placeholder="Enter email">--}}
{{--                </div>--}}
{{--                <button type="submit" class="btn btn-primary">Subscribe</button>--}}
{{--            </form>--}}
        </div><!--//container-->
    </section>
    <section class="blog-list px-3 py-5 p-md-5">
        <div class="container">
        @isset($posts)
            @foreach($posts as $post )
                <div class="item mb-5">
                    <div class="media">
                        @isset($post->image)
                            <img class="mr-3 img-fluid post-thumb d-none d-md-flex" src="{{asset('storage/'.$post->image)}}" alt="image">
                        @endisset
                        <div class="media-body">
{{--                            <h3 class="title mb-1">{{$post->user->name}}</h3>--}}
                            <h3 class="title mb-1"><a href="{{route('detailsPost',$post->id)}}">{{isset( $post->title) ?$post->title:""}}</a></h3>
                            <div class="meta mb-1"><span class="date">Published {{isset( $post->created_at) ?$post->created_at->diffForHumans():""}}</span>
                                <span class="time"> Replies: {{$post->replies->count() }}</span>
                            </div>
                            <div class="intro">{{isset( $post->body) ? \Illuminate\Support\Str::limit($post->body, 300):""}}</div>

                            <a class="more-link" href="{{route('detailsPost',$post->id)}}">Read more &rarr;</a>
                        </div><!--//media-body-->
                    </div><!--//media-->
                </div><!--//item-->

            @endforeach
                {{$posts->appends(request()->query())->links()}}
        @endisset

        </div>
    </section>


@include('footer')
