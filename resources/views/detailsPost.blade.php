@include('header')

<div class="main-wrapper">
    @include('dashboard.partials._session')


    <article class="blog-post px-3 py-5 p-md-5">
        <div class="container">
            <header class="blog-post-header">
                <h2 class="title mb-2">{{isset($post)?$post->title:''}}</h2>
                <div class="meta mb-3"><span class="date">Published {{isset( $post->created_at) ?$post->created_at->diffForHumans():""}}</span><span class="comment"><a href="#">{{isset($post)?$post->replies->count():'' }} replies</a></span></div>
            </header>

            <div class="blog-post-body">
                <figure class="blog-banner">
                    @isset($post->image)
                        <a href="https://made4dev.com"><img class="img-fluid" style="border: #00E578 solid 2px;" src="{{asset('storage/'.$post->image)}}" alt="image"></a>
                    @endisset
                </figure>
                <p>
                    {{$post->body}}
                </p>

            </div>

            <div class="tile mb-4">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="mt-5 mb-3">Enter Your Reply</h3>
                        <form action="{{route('sendReply')}}" method="post">
                            @csrf
                            @if(isset($reply))
                                @method('put')
                            @else
                                @method('post')
                            @endif

                            @include('dashboard.partials._errors')

                            <div class="form-group">
                                <label>{{__('site.name')}} :</label>
                                <input type="text" name="name" class="form-control" value="{{isset($reply)?$reply->name:""}}">
                            </div>

                            <div class="form-group">
                                <label>{{__('site.email')}} :</label>
                                <input type="text" name="email" class="form-control" value="{{isset($reply)?$reply->email:""}}">
                            </div>

                            <div class="form-group">
                                <input hidden type="text" name="post_id" class="form-control" value="{{isset($post)?$post->id:""}}">
                            </div>

                            <div class="form-group">
                                <label>{{__('site.message')}} :</label>
                                <textarea name="message" cols="30" rows="10"  class="form-control">{{isset($reply)?$reply->message:""}}</textarea>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-plus"></i>
                                       إرسال
                                </button>
                            </div>
                        </form>

                    </div>{{-- end-of-col-12 --}}
                </div>{{--end-of-row--}}


            </div>{{--end-of-tile mb-4--}}


{{--            <div class="blog-comments-section">--}}
{{--                <div id="disqus_thread"></div>--}}
{{--                <script>--}}
{{--                    /**--}}
{{--                     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT--}}
{{--                     *  THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR--}}
{{--                     *  PLATFORM OR CMS.--}}
{{--                     *--}}
{{--                     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT:--}}
{{--                     *  https://disqus.com/admin/universalcode/#configuration-variables--}}
{{--                     */--}}
{{--                    /*--}}
{{--                    var disqus_config = function () {--}}
{{--                        // Replace PAGE_URL with your page's canonical URL variable--}}
{{--                        this.page.url = PAGE_URL;--}}

{{--                        // Replace PAGE_IDENTIFIER with your page's unique identifier variable--}}
{{--                        this.page.identifier = PAGE_IDENTIFIER;--}}
{{--                    };--}}
{{--                    */--}}

{{--                    (function() {  // REQUIRED CONFIGURATION VARIABLE: EDIT THE SHORTNAME BELOW--}}
{{--                        var d = document, s = d.createElement('script');--}}

{{--                        // IMPORTANT: Replace 3wmthemes with your forum shortname!--}}
{{--                        s.src = 'https://3wmthemes.disqus.com/embed.js';--}}

{{--                        s.setAttribute('data-timestamp', +new Date());--}}
{{--                        (d.head || d.body).appendChild(s);--}}
{{--                    })();--}}
{{--                </script>--}}
{{--                <noscript>--}}
{{--                    Please enable JavaScript to view the--}}
{{--                    <a href="https://disqus.com/?ref_noscript" rel="nofollow">--}}
{{--                        comments powered by Disqus.--}}
{{--                    </a>--}}
{{--                </noscript>--}}
{{--            </div><!--//blog-comments-section-->--}}

        </div><!--//container-->
    </article>

</div><!--//main-wrapper-->


@include('footer')
