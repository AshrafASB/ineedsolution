<?php

use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('welcome');
})->name('welcome');


Auth::routes();
Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider')->where('provider','facebook|google|youtube');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback')->where('provider','facebook|google|youtube');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'LandingPageController@Landing')->name('welcome');

Route::get('/detailsPost/{post}', ['as' => 'detailsPost', 'uses' => 'LandingPageController@detailsPost']);
Route::post('/sendReply', ['as' => 'sendReply', 'uses' => 'LandingPageController@store']);
//Route::get('/detailsPost/{post}', 'LandingPageController@detailsPost')->name('detailsPost');
Route::post('/contact', 'LandingPageController@contact')->name('contact');











//

//Route::post('/store', 'LandingPageController@store')->name('store');
//Route::get('/requestNeedy', 'LandingPageController@requestNeedy')->name('requestNeedy');
//Route::post('/requestNeedyStore', 'LandingPageController@requestNeedyStore')->name('requestNeedyStore');

////Route::get('/request', function () {
////    return view('request');
////})->name('request');
//
//
//Route::get('/booking', function () {
//    return view('booking');
//})->name('booking');
//
////Route::get('/donation', 'LandingPageController@donation');
//
//
//
//Route::get('/thanks', function () {
//    return view('thanks');
//})->name('thanks');
